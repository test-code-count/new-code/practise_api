package server

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/spf13/viper"
)

func Initial() {
	if os.Getenv("ENV") == "DEV" {
		viper.SetConfigName("dev")
	} else {
		viper.SetConfigName("local")
	}
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	fmt.Println("environment  = " + viper.GetString("app.env"))

	port := fmt.Sprintf(":%s", viper.GetString("app.port"))
	http.ListenAndServe(port, nil)
}
