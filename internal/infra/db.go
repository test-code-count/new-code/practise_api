package infra

import (
	"fmt"
	"strconv"
	"time"

	"github.com/Supawit21/practice_api/internal/core/domain"
	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func InitDatabase() *gorm.DB {
	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		viper.GetString("postgres.host"),
		viper.GetString("postgres.port"),
		viper.GetString("postgres.user"),
		viper.GetString("postgres.password"),
		viper.GetString("postgres.name"),
		viper.GetString("postgres.ssl_mode"),
	)
	dbmaxconn, _ := strconv.Atoi(viper.GetString("postgres.max_connection"))
	dbmaxidleconn, _ := strconv.Atoi(viper.GetString("postgres.max_idle_connection"))
	dbmaxlifetimeconn, _ := strconv.Atoi(viper.GetString("postgres.max_lifetime_connection"))

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		SkipDefaultTransaction: true,
	})

	if err != nil {
		panic(err)
	}

	sqlDB, err := db.DB()
	if err != nil {
		panic(err)
	}
	sqlDB.SetMaxOpenConns(dbmaxconn)
	sqlDB.SetMaxIdleConns(dbmaxidleconn)
	sqlDB.SetConnMaxLifetime(time.Minute * time.Duration(dbmaxlifetimeconn))

	db.AutoMigrate(domain.User{})

	return db
}
