package ports

import "github.com/Supawit21/practice_api/internal/core/domain"

type UserRepository interface {
	Register(*domain.User) error
}
