package domain

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	ID            uuid.UUID      `gorm:"primaryKey" json:"id"`
	CreatedAt     time.Time      `gorm:"created_at" json:"-"`
	UpdatedAt     time.Time      `gorm:"updated_at" json:"-"`
	DeletedAt     gorm.DeletedAt `gorm:"deleted_at" json:"-"`
	Email         string         `gorm:"uniqueIndex:idx_email" json:"email" validate:"required,email,lte=255"`
	Username      string         `gorm:"uniqueIndex:idx_username" json:"username" validate:"required,lte=64"`
	Firstname     string         `json:"firstname" validate:"required,lte=255"`
	Lastname      string         `json:"lastname" validate:"required,lte=255"`
	Password      string         `json:"-" validate:"required,lte=64"`
	Mobile        *string        `json:"mobile"`
	MobileCountry *string        `json:"mobile_country"`
	Country       *string        `json:"country"`
	Nationality   *string        `json:"nationality"`
	DateOfBirth   time.Time      `json:"date_of_birth"`
	Gender        uint8          `json:"gender"`
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.New()
	u.CreatedAt = time.Now()
	return
}

func (u *User) BeforeUpdate(tx *gorm.DB) (err error) {
	u.UpdatedAt = time.Now()
	return
}
