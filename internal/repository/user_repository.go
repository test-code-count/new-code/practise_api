package repository

import (
	"github.com/Supawit21/practice_api/internal/core/domain"
	"gorm.io/gorm"
)

type UserRepository struct {
	*gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return UserRepository{DB: db}
}

func (r UserRepository) Register(u *domain.User) error {
	return nil
}
